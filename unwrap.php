<?php

require_once "vendor/autoload.php";
require_once "config.php";

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$log = new Logger('putio-unwrapper');

$log->addInfo('Starting Application');

try {
    $api = new PutIO\API($config['token']);


    $files = $api->files->listall($config['folder']);

    if (count($files) > 0) {
        foreach($files as $file) {
            if ($file['content_type'] == 'application/x-directory') {
                $log->addInfo("Checking folder",["folder" => $file['name']]);
                $inner_files = $api->files->listall($file['id']);
                foreach($inner_files as $inner_file) {
                    if (strpos($inner_file['content_type'],'video/') === 0) {
                        $log->addInfo("Moving file to base folder",['file' => $inner_file['name']]);
                        $api->files->move([$inner_file['id']],$config['folder']);
                        $log->addInfo('Removing folder',['folder' => $file['name']]);
                        $api->files->delete([$file['id']]);
                        break;
                    }
                }
            }
        }
    } else {
        $log->addWarning('No files found');
    }
} catch (Exception $e) {
    $log->addError('Failed to excute API.',['error' => $e->getMessage()]);
}